/**
 * Copyright (C) 2019, Roman Panov (roman.a.panov@gmail.com).
 */

package org.roman.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Graph<Vertex> {
  private final boolean myIsDirected;
  private final Map<Vertex, Set<Vertex>> myVertexToAdjacentVertices = new HashMap<>();

  public int countVertices() {
    return myVertexToAdjacentVertices.size();
  }

  public Graph(final boolean isDirected) {
    myIsDirected = isDirected;
  }

  /**
   * Adds an isolated vertex if it's not yet in the graph.
   *
   * @param vertex - the vertex to be added.
   * @return true if this graph changed as a result of the call.
   */
  public boolean addVertex(final Vertex vertex) {
    if (vertex == null) {
      return false;
    }

    if (myVertexToAdjacentVertices.containsKey(vertex)) {
      return false;
    }

    myVertexToAdjacentVertices.put(vertex, new HashSet<>());
    return true;
  }

  /**
   * Adds edge between two vertices. Vertices are added to the graph if needed.
   *
   * @param fromVertex
   * @param toVertex
   * @return true if this graph changed as a result of the call.
   */
  public boolean addEdge(final Vertex fromVertex, final Vertex toVertex) {
    if (fromVertex == null || toVertex == null) {
      return false;
    }

    boolean changed = false;
    Set<Vertex> fromAdjVertices = myVertexToAdjacentVertices.get(fromVertex);
    Set<Vertex> toAdjVertices = myVertexToAdjacentVertices.get(toVertex);

    if (fromAdjVertices == null) {
      fromAdjVertices = new HashSet<>();
      myVertexToAdjacentVertices.put(fromVertex, fromAdjVertices);
      changed = true;
    }

    if (toAdjVertices == null) {
      toAdjVertices = new HashSet<>();
      myVertexToAdjacentVertices.put(toVertex, toAdjVertices);
      changed = true;
    }

    if (fromAdjVertices.add(toVertex)) {
      changed = true;
    }

    if (!myIsDirected) {
      if (toAdjVertices.add(fromVertex)) {
        changed = true;
      }
    }

    return changed;
  }

  public List<Vertex> getPath(final Vertex fromVertex, final Vertex toVertex) {
    if (fromVertex == null || toVertex == null) {
      return Collections.emptyList();
    }

    if (
      !myVertexToAdjacentVertices.containsKey(fromVertex) ||
      !myVertexToAdjacentVertices.containsKey(toVertex)) {
      return Collections.emptyList();
    }

    final List<Vertex> path = new ArrayList<>();

    if (findPath(fromVertex, toVertex, new HashSet<>(), path)) {
      Collections.reverse(path);
      return path;
    }

    return Collections.emptyList();
  }

  private boolean findPath(
    final Vertex fromVertex, final Vertex toVertex,
    final Set<Vertex> visitedVertices, final List<? super Vertex> path) {
    assert fromVertex != null;

    if (toVertex == null) {
      return false;
    }

    if (fromVertex == toVertex) {
      path.add(toVertex);
      return true;
    }

    if (visitedVertices.contains(fromVertex)) {
      return false;
    }

    final Set<Vertex> adjVertices = myVertexToAdjacentVertices.get(fromVertex);

    if (adjVertices == null) {
      return false;
    }

    visitedVertices.add(fromVertex);

    for (final Vertex adjVertex : adjVertices) {
      if (findPath(adjVertex, toVertex, visitedVertices, path)) {
        path.add(fromVertex);
        return true;
      }
    }

    return false;
  }
}
