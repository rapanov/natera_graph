/**
 * Copyright (C) 2019, Roman Panov (roman.a.panov@gmail.com).
 */

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.roman.util.Graph;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class GraphTest {
  @Parameterized.Parameters
  public static Collection<Boolean> parameters() {
    return Arrays.asList(false, true);
  }

  @Parameterized.Parameter
  public boolean myIsDirected;

  @Test
  public void testAddVertex() {
    final Graph<Integer> graph = new Graph<>(myIsDirected);

    assertTrue(graph.addVertex(20));
    assertTrue(graph.addVertex(30));
    assertTrue(graph.addVertex(10));
    assertTrue(graph.addVertex(10000));
    assertTrue(graph.addVertex(0));
    assertFalse(graph.addVertex(null));
    assertFalse(graph.addVertex(10));
    assertEquals(5, graph.countVertices());
  }

  @Test
  public void testAddEdge() {
    final Graph<Integer> graph = new Graph<>(myIsDirected);

    assertTrue(graph.addEdge(20, 30));
    assertTrue(graph.addEdge(30, 40));
    assertTrue(graph.addEdge(40, 50));
    assertTrue(graph.addEdge(50, 30));
    assertTrue(graph.addEdge(30, 60));
    assertFalse(graph.addEdge(20, 30));
    assertFalse(graph.addEdge(50, 30));
    assertEquals(myIsDirected, graph.addEdge(30, 50));
    assertFalse(graph.addEdge(null, null));
    assertFalse(graph.addEdge(null, 1));
    assertFalse(graph.addEdge(2, null));
    assertEquals(5, graph.countVertices());
  }

  @Test
  public void testGetPath() {
    Graph<Integer> graph = new Graph<>(myIsDirected);
    graph.addVertex(20);
    graph.addVertex(50);

    assertTrue(graph.getPath(20, 30).isEmpty());

    graph = new Graph<>(myIsDirected);
    graph.addEdge(20, 30);
    List<Integer> path = graph.getPath(20, 30);

    assertEquals(2, path.size());
    assertEquals(20, path.get(0).intValue());
    assertEquals(30, path.get(1).intValue());

    path = graph.getPath(30, 20);

    if (myIsDirected) {
      assertTrue(path.isEmpty());
    }
    else {
      assertEquals(2, path.size());
      assertEquals(30, path.get(0).intValue());
      assertEquals(20, path.get(1).intValue());
    }

    graph = new Graph<>(myIsDirected);
    graph.addEdge(20, 30);
    graph.addEdge(30, 40);
    graph.addEdge(40, 50);
    graph.addEdge(50, 30);
    graph.addEdge(30, 60);
    path = graph.getPath(20, 60);

    assertFalse(path.isEmpty());
    assertEquals(20, path.get(0).intValue());
    assertEquals(60, path.get(path.size() - 1).intValue());

    path = graph.getPath(60, 20);

    if (myIsDirected) {
      assertTrue(path.isEmpty());
    }
    else {
      assertFalse(path.isEmpty());
      assertEquals(60, path.get(0).intValue());
      assertEquals(20, path.get(path.size() - 1).intValue());
    }

    assertTrue(graph.getPath(null, null).isEmpty());
    assertTrue(graph.getPath(null, 30).isEmpty());
    assertTrue(graph.getPath(40, null).isEmpty());
    assertTrue(graph.getPath(40, 90).isEmpty());

    path = graph.getPath(30, 50);

    assertFalse(path.isEmpty());

    if (myIsDirected) {
      assertEquals(30, path.get(0).intValue());
      assertEquals(40, path.get(1).intValue());
      assertEquals(50, path.get(2).intValue());
    }
    else {
      assertEquals(30, path.get(0).intValue());
      assertEquals(50, path.get(path.size() - 1).intValue());
    }
  }
}
